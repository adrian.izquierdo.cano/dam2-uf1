import java.util.Comparator;

public class Estudiante implements Comparable<Estudiante>{
	String nombre;
	int edad;
	int curso;
	
	public Estudiante(String nombre, int edad, int curso) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.curso = curso;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}


	public int getCurso() {
		return curso;
	}


	public void setCurso(int curso) {
		this.curso = curso;
	}


	public static final Comparator<Estudiante> COMPARA_EDAT = 
			new Comparator<Estudiante>() {
		@Override
		public int compare(Estudiante e1, Estudiante e2) {
			if(e2.getEdad() - e1.getEdad() == 0)
			{
				return e1.nombre.compareTo(e2.nombre);
			}
			return e2.getEdad() - e1.getEdad();
		}
	};
	
	public static final Comparator<Estudiante> COMPARA_CURSO = 
			new Comparator<Estudiante>() {
		@Override
		public int compare(Estudiante e1, Estudiante e2) {
			if (e2.getCurso() - e1.getCurso() == 0)
			{
				return e1.nombre.compareTo(e2.nombre);
			}
			return e2.getCurso() - e1.getCurso();
		}
	};
	
	@Override
    public int compareTo(Estudiante e) 
	{
        return this.nombre.compareTo(e.nombre);
    }
	
	@Override
    public String toString() {
        return "\nEstudiante-> Nombre: "+this.nombre+" Edad: "+this.edad+" Curso: "+this.curso ;}	
}

import java.util.ArrayList;
import java.util.Collections;


public class Main {

	public static void main(String[] args) {

		ArrayList<Estudiante> list = new ArrayList<Estudiante>();
		list.add(new Estudiante("Carlos",19,3));
		list.add(new Estudiante("Felipe",21,1));
		list.add(new Estudiante("Ana",18,4));
		list.add(new Estudiante("Rosa",15,1));
		list.add(new Estudiante("Jose",10,2));
		/*Sin ordenar*/
		System.out.println(list);

		/*Ordenado alfabeticamente*/
		Collections.sort(list);
		System.out.println(list);
		
		/*Ordenado edad*/
		Collections.sort(list, Estudiante.COMPARA_EDAT);
		System.out.println(list);
		
		/*Ordenado curso*/
		Collections.sort(list, Estudiante.COMPARA_CURSO);
		System.out.println(list);
		
	}

}

package Comparators;

public enum Cosas {
	CAJAS, NO_CAJAS, PUEDE_QUE_CAJAS
}

class Cosa
{
	public String hacerCosas(Cosas tipoDeCosa)
	{
		switch(tipoDeCosa)
		{
			case CAJAS:
				return "Una caja";
			case NO_CAJAS:
				return "Una no-caja";
			case PUEDE_QUE_CAJAS:
				return "Una posible-caja";
		}
		
		return null;
	}
}
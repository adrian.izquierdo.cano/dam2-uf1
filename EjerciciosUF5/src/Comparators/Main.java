package Comparators;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

	public static void main(String[] args) 
	{
		ArrayList<Persona> list = new ArrayList<Persona>();
		
		list.add(new Persona("Adrian", 21, 172));
		list.add(new Persona("D:", 20, 171));
		list.add(new Persona(":l", 12, 146));
		list.add(2, new Persona(":l", 12, 146));
		
		System.out.println(list);
		
		list.remove(list.get(1));
		
		Collections.sort(list);
		System.out.println(list);
		
		Collections.sort(list, Persona.COMPARAR_EDAT);
		System.out.println(list);
	}

}

    package Comparators;

import java.util.Comparator;
import java.util.Date;

public class Persona implements Comparable<Persona> //compareTo
{
	private String nom;
	private int edat;
	private int alcada;
	private Date fecha;
	
	public Persona()
	{
		nom="Unknown";
		edat=18;
		alcada=170;
		fecha= new Date();
	}
	
	public Persona(String nom, int edat, int alcada)
	{
		setNom(nom);
		setEdat(edat);
		setAlcada(alcada);
		setFecha(new Date());
	}
	
	

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public int getAlcada() {
		return alcada;
	}

	public void setAlcada(int alcada) {
		this.alcada = alcada;
	}
	
	
	public Date getFecha() {
		
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString()
	{
		
		return "Persona" + getNom() + getEdat() + getAlcada() + getFecha() ;
		
	}
	@Override
	public int compareTo(Persona p)
	{
		
		return this.getAlcada() - p.getAlcada();
	}
	
	public static final Comparator<Persona> COMPARAR_EDAT = new Comparator<Persona>()
	{
		@Override
		public int compare(Persona p1, Persona p2)
		{
			return p1.getEdat() - p2.getEdat();
		}
		
	};
	
	public int compareTo1(Persona p)
	{
		
		return this.fecha.compareTo(p.fecha);
	}
	
}

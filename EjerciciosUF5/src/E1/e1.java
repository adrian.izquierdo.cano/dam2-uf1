package E1;

import java.awt.List;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

public class e1 {

	public static void main(String[] args) 
	{
		
		Deque pila = new ArrayDeque();
		Deque pila2 = new LinkedList();
		
		pila.addFirst(1);
		pila.addFirst(2);
		pila.addFirst(3);
		pila.add(5);
		pila.addFirst(4);
		pila.add(2);
		pila.push(50);
		pila.push(25);
		System.out.println(pila.peek());
		pila.pop();
		System.out.println(pila.peek());
		
		//push y pop para pilas
		//add y pop para colas
		
		pila2.add(1);
		pila2.add(2);
		pila2.add(3);
		pila2.add(4);
		pila2.addFirst(2);
		
		
		
		
		System.out.println(pila);
		System.out.println(pila2);
		// depende de como trates el DEQUE es una pila o un cola.
		//cola metes por un lado sacas por otro.
		//pila sacas y metes del mismo sitio.

	}

}

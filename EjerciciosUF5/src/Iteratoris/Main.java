package Iteratoris;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) 
	{
	
		String[] cadena = {"q","b","d","a","b","q","q"};

		ArrayList<String> list = new ArrayList<>(Arrays.asList(cadena));
		
		
		eliminar1(list,"q");
		mostrar(list);
		System.out.println(list);
		
		
		eliminar(list,"q");
		mostrar(list);
		System.out.println(list);
		
	}
	
	public static void eliminar (ArrayList<String> list, String ko) 
	{
		Iterator<String> it = list.iterator();
		
		while (it.hasNext())
		{
			if(it.next().equals(ko)) 
			{
				it.remove();
			}
			
		}
		
	}
	
	public static void eliminar1 (ArrayList<String> list, String ko) 
	{
		Iterator<String> it = list.iterator();
		
		while (it.hasNext())
		{
			if(it.next().equals(ko)) 
			{
				it.remove();
				break;
			}
			
		}
		
	}
	
	public static void mostrar (ArrayList<String> list)
	{
		for (String s : list)
		{
			System.out.println(s);
		}
	}
	
}

class OurIterator implements Iterator
{

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object next() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void remove()
	{
		
	}
}

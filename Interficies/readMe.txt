Hay 5 clases voy a explicar las cosas mas importantes.

Main: Hay varias pruebas ya hechas para que se vea que funciona todo lo que se pide. Ademas hay varios metodos, sortTipoArma
basicamente a partir de una lista de armas la mete en un treeset y de esta forma se ordena todo segun el comparable 
y el comparator, luego la muestra por consola. Los otros dos metodos son para clonar cargadores, tu pasas un cargador y te
duplica el cargador y el Deque que tiene el cargador.

Arma: Tiene tanto comparable como comparator. El comparator simplemente sirve para que compare por nombre de base con el compareTo.
El comparable por otro lado ordena segun un enum que he hecho, cada elemento del enum tiene un indice y este indice permite
comparar segun lo que he decidido, este enum hace referencia al tipo de arma.
El Arma ademas tiene varios metodos: ponerCargador intenta poner un cargador de genericos si el tipo de municion es 
correcto lo pone sino no. quitarCargador es simplemente para poder quitar los cargadores ya que cada arma solo puede tener 1
cargador. Disparar dispara el arma si hay cargador y si este tiene balas, ademas depende de que tipo de municion tenga hace un 
sonido distinto representado con un syso.

Cargador: Usa genericos, basicamente contiene un deque que es de generico y que de esta forma los cargadores pueden tener balas
o cartuchos. Ademas el metodo add esta modificado para que tenga un limite.

Por ultimo Bala y Cartucho son los tipos que se usan para el cargador. Se pueden clonar y tienen un toString para saber que
tipo de municion es.

import java.util.Comparator;

enum tipoArma implements Comparable<tipoArma>
{
	P("Pistol",1), SMG("SubMachineGun",2),S("Shotgun",3), AR("AssaultRifle",4),LMG("LightMachineGun",5),SR("SniperRifle",6);
	
	private String name;
	private int index;
	
	tipoArma(String name, int index)
	{
		this.name = name;
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}	
	
}

public class Arma implements Comparable<Arma>, Comparator<tipoArma> {
	
	private String nombre;
	private String tipoArma;
	private String tipoMunicion;
	private Cargador<?> cargador;
	
	

	public Arma(String nombre,String tipoArma, String tipoMunicion, Cargador<?> cargador) {
		super();
		this.nombre = nombre;
		this.tipoArma = tipoArma;
		this.tipoMunicion = tipoMunicion;
		this.cargador = cargador;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String tipoMunicion() {
		return tipoMunicion;
	}

	public void settipoMunicion(String tipoMunicion) {
		this.tipoMunicion = tipoMunicion;
	}

	public String getTipoArma() {
		return tipoArma;
	}

	public void setTipoArma(String tipoArma) {
		this.tipoArma = tipoArma;
	}
	
	public Cargador<?> getCargador() {
		return cargador;
	}

	public void setCargador(Cargador<?> cargador) {
		this.cargador = cargador;
	}

	public void ponerCargador(Cargador<?> car)
	{
		if (this.cargador == null)
		{
			if(car.getBalas().peekLast().toString().contentEquals(tipoMunicion))
			{
				setCargador(car);
				System.out.println("Poniendo cargador");
			}
			else
			{
				System.out.println("Las balas del cargador no son las adecuadas");
			}
			
		}
		else
		{
			System.out.println("Ya hay un cargador");
		}
	}
	
	public void quitarCargador()
	{
		if (this.cargador != null)
		{
			this.cargador = null;
			System.out.println("Quitando cargador");
		}
		else
		{
			System.out.println("No hay cargador para quitar");
		}
	}
	
	public void disparar()
	{
		if (this.cargador == null)
		{
			
			System.out.println("No hay cargador!!!");
		}
		else if (cargador.getBalas().peek() == null)
		{
			System.out.println("Necesito municion!!!");
		}
		
		else if (cargador.getBalas().peek() != null)
		{
			if (cargador.getBalas().peek() instanceof Cartucho)
			{
				this.cargador.getBalas().pollLast();
				System.out.println("Pum");
			}
			else if (this.cargador.getBalas().peek() instanceof Bala)
			{
				this.cargador.getBalas().pollLast();
				System.out.println("Pium");
			}
			
		}
	}
	
	@Override
	public int compareTo(Arma a) {
		return this.nombre.compareTo(a.nombre);
	}

	@Override
	public int compare(tipoArma o1, tipoArma o2) {
		int returnValue = 0;
	      if (o1.getIndex() > o2.getIndex()) {
	          returnValue = 1;
	      } else if (o1.getIndex() < o2.getIndex()) {
	          returnValue = -1;
	      }
	      return returnValue;
	}
	
	@Override
    public String toString() 
	{
        return "Nombre: "+this.nombre+" Tipo Arma: "+this.tipoArma+" Tipo municion: "+this.tipoMunicion+" Cargador: "+this.cargador;
    }	

	
}


public class Bala implements Cloneable{
	
	private String tipo;

	public Bala(String tipo) {
		super();
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public Bala clone() {
		try {
			return (Bala)super.clone();	
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
    public String toString() 
	{
        return this.tipo;
    }	
}

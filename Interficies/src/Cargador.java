import java.util.ArrayDeque;
import java.util.Deque;

public class Cargador<T> implements Cloneable {
	
	private Deque<T> balas = new ArrayDeque<T>();
	private int limite;
	
	
	public Cargador(Deque balas, int limite) {
		super();
		this.balas = balas;
		this.limite = limite;
	}

	public Deque<T> getBalas() {
		return balas;
	}

	public void setBalas(Deque<T> balas) {
		this.balas = balas;
	}

	public int getLimite() {
		return limite;
	}

	public void setLimite(int limite) {
		this.limite = limite;
	}
	
	public void add(T municion ) 
	{
		if (balas.size()<=limite) 
		{
			balas.add(municion);
		}else 
		{			
			System.out.println("El cargador esta lleno");
		}
	}
	
	@Override
	public Cargador<T> clone() {
		try {
			return (Cargador<T>)super.clone();	
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}

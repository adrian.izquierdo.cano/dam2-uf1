
public class Cartucho implements Cloneable{

	private String tipo;

	public Cartucho(String tipo) {
		super();
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public Cartucho clone() {
		try {
			return (Cartucho)super.clone();	
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
    public String toString() 
	{
        return this.tipo;
    }	
}

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		/*Creamos armas, cargadores,balas*/
		
		 List<Arma> listaArmas = new ArrayList<Arma>();
		 Arma b = new Arma("MP5", "SMG", "9mm", null);
		 Arma c = new Arma("M1911", "P", ".45", null);
		 Arma a = new Arma("Glock 17", "P", "9mm", null);
		 Arma d = new Arma("Spas 12", "S", "12", null);
		 listaArmas.add(b);
		 listaArmas.add(d);
		 listaArmas.add(c);
		 listaArmas.add(a);
		 
		 /*Ordenacion de armas ha partir del enum del tipo de arma*/
		 /*Ademas compareTo ordena alfabeticamente en caso de ser el mismo tipo de arma*/
		 sortTipoArma(listaArmas);
		 
		 
		 /*pila de balas(cargador)*/
		 Deque<Bala> pila1 = new ArrayDeque<Bala>();
		 /*Creamos una bala y la ponemos en la pila*/
		 Bala ba1 = new Bala("9mm");
		 pila1.add(ba1);
		 /*metemos las balas en el cargador*/
		 Cargador<Bala> car = new Cargador<Bala>(pila1, 30);
		 /*Clonamos cargador*/
		 Cargador<Bala> car1 = clonarCargadorBalas(car);
		 Cargador<Bala> car2 = clonarCargadorBalas(car);
		 
		 Deque<Cartucho> pila2 = new ArrayDeque<Cartucho>();
		 Cartucho c1 = new Cartucho("12");
		 pila2.add(c1);
		 Cargador<Cartucho> car3 = new Cargador<Cartucho>(pila2,7);
		 
		 a.ponerCargador(car);
		 a.disparar();
		 a.disparar();
		 b.quitarCargador();
		 b.ponerCargador(car1);
		 b.disparar();
		 b.disparar();
		 c.ponerCargador(car2);
		 d.ponerCargador(car3);
		 d.disparar();
	}
	/*Ordena una lista usando el enum*/
	public static void sortTipoArma(final List<Arma> listaArmas) {
        final Set<Arma> set = new TreeSet<Arma>();
        set.addAll(listaArmas);
        for (final Arma tarma : set) {
            System.out.println(tarma);
        }
    }
	
	public static Cargador<Bala> clonarCargadorBalas(Cargador<Bala> cargador)
	{
		Deque<Bala> pila = cargador.getBalas();
		Deque<Bala> pilaClone = new ArrayDeque<Bala>(pila.size());
		for (Bala item : pila) 
	    { 
			pilaClone.add(item.clone());
	    }
		Cargador<Bala> cargadorClone = cargador.clone();
		cargadorClone.setBalas(pilaClone);
		return cargadorClone;	
	}
	
	public static Cargador<Cartucho> clonarCargadorCartucho(Cargador<Cartucho> cargador)
	{
		Deque<Cartucho> pila = cargador.getBalas();
		Deque<Cartucho> pilaClone = new ArrayDeque<Cartucho>(pila.size());
		for (Cartucho item : pila) 
	    { 
			pilaClone.add(item.clone());
	    }
		Cargador<Cartucho> cargadorClone = cargador.clone();
		cargadorClone.setBalas(pilaClone);
		return cargadorClone;	
	}
}

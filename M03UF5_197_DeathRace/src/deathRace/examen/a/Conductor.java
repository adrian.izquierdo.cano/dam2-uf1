package deathRace.examen.a;

import java.util.Arrays;
import java.util.Deque;

public abstract class Conductor implements Constants, Comparable<Conductor>  {
	
	private String nom;
	private int casella;
	private int gasolina;
	private ConductorEstat estat;
	private int aturades;
	
	Conductor(String nom){
		this.nom = nom;
		casella = 0;
		estat = ConductorEstat.GO;
		aturades = 0;
		gasolina = GASOLINA;
	}
	
	public String getNom(){
		return nom;
	}
	
	public void setGasolina(int gasolina) {
		this.gasolina += gasolina;
		if (this.gasolina > GASOLINA) this.gasolina = GASOLINA;
	}
	
	public int getGasolina() {
		return gasolina;
	}
	
	public void gastaGasolina() {
		gasolina --;
	}
	
	public void setCasella(int casella) {
		this.casella = casella;
	}
	
	public int getCasella() {
		return (casella);
	}

	public void aturar() {
		this.aturades ++;
	}
	
	public int getAturades() {
		return (aturades);
	}

	
	public int avan�a (int av) {
		casella = casella + av;
		return casella;
	}
	
	public void setEstat (ConductorEstat ce) {
		this.estat = ce;
	}
	
	public ConductorEstat getEstat() {
		return estat;
	}
	
	public abstract void colisio(Deque <Conductor> boxes);	
	
	public abstract void repostar(Deque <Conductor> boxes);
	
	@Override
	public String toString()
	{
		return nom + " Casella: " + casella + " Estat: " + estat + " Gasolina: " + gasolina + " Aturaades " + aturades;	
	}
	
	@Override
	public int compareTo(Conductor c) 
	{
		if(this.estat.equals(ConductorEstat.GO) || this.estat.equals(ConductorEstat.BOXES))
		{
			if(c.estat.equals(ConductorEstat.GO) || c.estat.equals(ConductorEstat.BOXES))
			{
				if(this.casella > c.casella)
				{
					return 1;
				}
				else if(this.casella == c.casella)
				{
					if(this.aturades < c.aturades)
					{
						return 1;
					}
					else if(this.aturades == c.aturades)
					{
						return this.nom.compareTo(c.nom);
					}
					else
					{
						return -1;
					}
				}
				else
				{
					return -1;
				}
			}
			else if(c.estat.equals(ConductorEstat.OUT))
			{
				return 1;
			}
		}
		else if(this.estat.equals(ConductorEstat.OUT))
		{
			if(c.estat.equals(ConductorEstat.GO) || c.estat.equals(ConductorEstat.BOXES))
			{
				return -1;
			}
			else if(c.estat.equals(ConductorEstat.OUT))
			{
				if(this.casella > c.casella)
				{
					return 1;
				}
				else if(this.casella == c.casella)
				{
					if(this.aturades < c.aturades)
					{
						return 1;
					}
					else if(this.aturades == c.aturades)
					{
						return this.nom.compareTo(c.nom);
					}
					else
					{
						return -1;
					}
				}
				else
				{
					return -1;
				}
			}
			
		}
		return 0;
	}
}

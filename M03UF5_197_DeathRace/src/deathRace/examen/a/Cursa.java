package deathRace.examen.a;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

public class Cursa implements Constants{
	private static final Random random = new Random();	
	private int totalCaselles;
	private List<Conductor> conductors = new ArrayList<Conductor>();
	private Deque<Conductor> boxes = new ArrayDeque<Conductor>();
//conductors �s la llista de conductors
//boxes �s la cua de conductors amb problemes
	public Cursa() {
		super();
	}
	Cursa(int nCaselles, int pilots, int kamikazes)
	{
		this.totalCaselles = nCaselles;
		
		for (int i = 0; i < pilots; i++) {
			conductors.add(new Pilot("P"+i));
		}
		for (int i = 0; i < kamikazes; i++) {
		
			conductors.add(new Kamikaze("K"+i));
		}
	}
	
	public void torn() {
		int k=0;
		int pos;
		Conductor pil=null;
		
		for (Conductor c : conductors) {
			if (c.getEstat() == ConductorEstat.GO) {
				if (c instanceof Competidor)
					k = random.nextInt(TIRADA_COMPETIDOR)+1;
				if (c instanceof Animador)
					k = random.nextInt(TIRADA_ANIMADOR)+1;
				
				pos = c.avan�a(k);
				if (pos <= totalCaselles) {
					
					for (Conductor conductor : conductors) 
					{
						if(c != conductor && c.getCasella() == conductor.getCasella() && c.getEstat().equals(ConductorEstat.GO) && conductor.getEstat().equals(ConductorEstat.GO))
						{
							conductor = pil;
						}
					}		
					
					if (pil != null) 
					{
						pil.colisio(boxes);
							
					}
					c.gastaGasolina();
					if (c.getGasolina() == 0)
						c.repostar(boxes);
				}
			}
			
		}
	
		this.gestioBoxes();
	}

	private void gestioBoxes()
	{
		System.out.println(this.boxes);
		if(boxes.peekFirst() != null)
		{
			boxes.peekFirst().setEstat(ConductorEstat.GO);
			boxes.pollFirst().setGasolina(Constants.GASOLINA);
		}
	}
	
	
	public boolean acabada() 
	{
		boolean todosMuertos = false;
		for (Conductor c : conductors)
		{
			if(c.getCasella() > this.totalCaselles)
			{
				
				return true;
			}
			if(c.getEstat().equals(ConductorEstat.GO) || c.getEstat().equals(ConductorEstat.BOXES))
			{
				return false;
			}	
		}
		
		return true;
	}
	
	public void situacio()
	{
		int competidor = 0, animador = 0;
		int cGo = 0, cOut = 0, cBox = 0;
		int aGo = 0, aOut = 0, aBox = 0;
		this.conductors.sort(null);
		System.out.println(this.conductors);
		for (Conductor c : conductors)
		{
			if(c instanceof Competidor)
			{
				competidor++;
				if(c.getEstat().equals(ConductorEstat.GO))
				{
					cGo++;
				}
				else if(c.getEstat().equals(ConductorEstat.OUT))
				{
					cOut++;
				}
				else
				{
					cBox++;
				}
			}
			else
			{
				animador++;
				if(c.getEstat().equals(ConductorEstat.GO))
				{
					aGo++;
				}
				else if(c.getEstat().equals(ConductorEstat.OUT))
				{
					aOut++;
				}
				else
				{
					aBox++;
				}
			}
		}
		
		System.out.println("Competidors: " + competidor + " En cursa: " + cGo + " En boxes: " + cBox + " Out: " + cOut);
		System.out.println("Animador: " + animador + " En cursa: " + aGo + " En boxes: " + aBox + " Out: " + aOut);
	}
	
	@Override
	public String toString()
	{
		String s = null;
		for (Conductor conductor : conductors) 
		{
			s += "\n" + conductor.getNom() + " Casella: " + conductor.getCasella() + " Gasolina: " + conductor.getGasolina() + " Aturades: " + conductor.getAturades();
		}
		return s;
		
	}
}

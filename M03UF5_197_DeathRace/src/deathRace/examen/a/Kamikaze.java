package deathRace.examen.a;

import java.util.Deque;

public class Kamikaze extends Conductor implements Constants, Animador{
	
	private int impactes;
	
	Kamikaze(String nom) {
		super(nom);
		this.impactes = 0;
	}

	@Override 
	public void colisio(Deque <Conductor> boxes) 
	{
		this.aturar();
		this.impactes++;
		if(this.impactes >= Constants.IMPACTES)
		{
			this.setEstat(ConductorEstat.OUT);
		}
		else
		{
			this.setEstat(ConductorEstat.BOXES);
			boxes.addFirst(this);
		}
	}		
	
	@Override
	public void repostar(Deque <Conductor> boxes) 
	{
		this.aturar();
		this.setEstat(ConductorEstat.BOXES);
		boxes.addFirst(this);
	}
}

package deathRace.examen.a;

import java.util.Deque;

public class Pilot extends Conductor implements Constants, Competidor{
	
	Pilot(String nom) {
		super(nom);
	}
	
	@Override 
	public void colisio(Deque <Conductor> boxes) 
	{
		this.aturar();
		
		if(this.getAturades() >= Constants.ATURADES)
		{
			this.setEstat(ConductorEstat.OUT);
		}
		else
		{
			this.setEstat(ConductorEstat.BOXES);
			boxes.addLast(this);
		}
	}	
	
	@Override
	public void repostar(Deque <Conductor> boxes) 
	{
		this.colisio(boxes);
	}
}

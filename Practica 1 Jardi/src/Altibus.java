import java.util.Random;

public class Altibus extends Planta
{

	public Planta creix()
	{
		Planta ret = null;
		
		if(this.getAltura() < 10)
		{
			this.setAltura(this.getAltura() + 1);
			
			if(this.getAltura() > 7 && this.getAltura() < 10)
			{
				ret = new Llavor(new Altibus(), 5);
			}
		}
		else
		{
			this.setViva(false);
			return null;
		}
		
		return ret;
		
	}
	
	public char getChar(int nivell)
	{
		nivell++;
		if(nivell < this.getAltura())
		{
			return '|';
		}
		else if(nivell == this.getAltura())
		{
			return 'O';
		}
		else
		{
			return ' ';
		}
	}
	
	public int escampaLlavor()
	{
		Random rand = Principal.rand;
		
		int result =  rand.nextInt(this.getAltura() > 0 && this.getAltura() % 2 == 0 ? this.getAltura() - 1 : this.getAltura()) - this.getAltura() / 2;
		
		result = result == 0 ? result + (rand.nextBoolean() ? 1 : -1) : result;
		
		return result;
	}
}

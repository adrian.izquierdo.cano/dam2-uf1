import java.util.Random;

public class Declinus extends Planta
{
	private boolean creixent = true;
	private int step = 0;
	
	public Planta creix()
	{
		Planta ret = null;
		
		if(creixent)
		{
			step++;
			if(step == 2)
			{
				this.setAltura(this.getAltura() + 1);
				step = 0;
			}
		}
		else
		{
			step++;
			if(step == 2)
			{
				this.setAltura(this.getAltura() - 1);
				step = 0;
			}
		}
		
		if(this.getAltura() == 4 && step == 0)
		{
			creixent = false;
			ret = new Llavor(new Declinus(), 5);
		}
		else if(this.getAltura() == 3 && !creixent && step == 0)
		{
			ret = new Llavor(new Declinus(), 5);
		}
		else if(!creixent && this.getAltura() == 0)
		{
			this.setViva(false);
		}
		
		return ret;
		
	}
	
	public char getChar(int nivell)
	{
		nivell++;
		if(nivell < this.getAltura())
		{
			return ':';
		}
		else if(nivell == this.getAltura())
		{
			return '*';
		}
		else
		{
			return ' ';
		}
	}
	
	public int escampaLlavor()
	{
		Random rand = Principal.rand;
		
		int result = rand.nextInt(5) - 2;
		
		result = result == 0 ? result + (rand.nextBoolean() ? 2 : -2) : result;
		
		return result;
	}
}

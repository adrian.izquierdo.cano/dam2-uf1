import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Jardi {

	private volatile Planta[] jardi;
			
	public Jardi(int amplada, Planta[] plantes, int[] posicions)
	{
		if(plantes.length != posicions.length)
		{
			throw new IllegalArgumentException("Discrepancia numero plantas y numero posiciones");
		}
		
		jardi = new Planta[amplada];
		
		for(int i = 0; i < plantes.length; i++)
		{
			jardi[posicions[i]] = plantes[i];
		}
	}
	
	public void temps()
	{
		int[] posicions = new int[jardi.length];
		
		Utils.initArray(posicions);
		
		Utils.shuffleArray(posicions);
		
		for(int i = 0; i < posicions.length; i++)
		{
			int pos = posicions[i];
			
			Planta p = jardi[pos];
			
			if(p == null) continue;
			
			Planta l = p.creix();
			
			if(!p.esViva())
			{
				jardi[pos] = null;
			}
			
			if(l == null) continue;
			
			if(l instanceof Llavor)
			{
				int delta = p.escampaLlavor();

				if(pos + delta >= 0 && pos + delta < jardi.length)
				{
					boolean success = plantaLlavor(l, delta + pos);
					/*
					if(success)
					{
						System.out.println("Llavor plantada.");
					}
					else
					{
						System.out.println("Llavor NO plantada.");
					}
					*/
				}
			}
			else
			{
				jardi[pos] = l;
			}
		}
	}
		
	private boolean plantaLlavor(Planta novaPlanta, int pos)
	{
		if(jardi[pos] == null)
		{
			jardi[pos] = novaPlanta;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private int highest()
	{
		int maxHeight = -1;
		
		for(Planta p : jardi)
		{
			if(p != null && p.getAltura() > maxHeight)
			{
				maxHeight = p.getAltura();
			}
		}
		
		return maxHeight;
	}
	
	public String toString()
	{
		int h = highest();
		
		if(h < 0)
		{
			return "No hi ha plantes.";
		}
		
		char[][] jardiMatrix = new char[jardi.length][h + 1];
		
		for(int i = 0; i < jardiMatrix.length; i++)
		{
			for(int j = 0; j < jardiMatrix[i].length; j++)
			{
				jardiMatrix[i][j] = ' ';
			}
		}
		
		for(int x = 0; x < jardi.length; x++)
		{
			Planta p = jardi[x];
			
			if(p == null) continue;
			
			for(int y = 0; y < p.getAltura(); y++)
			{
				jardiMatrix[x][y] = p.getChar(y);
			}
		}
		
		String result = new String();
		
		for(int i = 0; i < 12 - h; i++)
		{
			result += '\n';
		}
		
		for(int y = jardiMatrix[0].length - 1; y >= 0; y--)
		{	
			for(int x = 0; x < jardiMatrix.length; x++)
			{
				result += jardiMatrix[x][y];
			}
			
			result += "\n";
		}
		
		for(int i = 0; i < jardi.length; i++)
		{
			if(jardi[i] != null && jardi[i] instanceof Llavor)
			{
				result += "^";
			}
			else
			{
				result += "_";
			}
		}
		
		result += "\n";
		
		return result;
	}
	
	public static int[] getRandomPositions(int quantity, int upperBound)
	{
		int[] allPositions = new int[upperBound];
		
		for(int i = 0; i < allPositions.length; i++)
		{
			allPositions[i] = i;
		}
		
		Utils.shuffleArray(allPositions);
		
		return Arrays.copyOf(allPositions, quantity);	
	}
	
	private int[] availablePos()
	{
		int count = 0;
		
		int[] positions = new int[jardi.length];
		
		for(int i = 0; i < jardi.length; i++)
		{
			if(jardi[i] == null)
			{
				positions[count] = i;
				count++;
			}
		}
		
		return Arrays.copyOf(positions, count);
	}
	
	public void afegirPlanta(Planta nova)
	{
		int[] positions = this.availablePos();
		
		if(positions.length > 0)
		{
			this.plantaLlavor(nova, positions[Principal.rand.nextInt(positions.length)]);
		}
	}
}

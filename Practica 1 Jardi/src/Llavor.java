
public class Llavor extends Planta
{
	private Planta planta;
	private int temps;
	private int currentTemps = 0;
	
	public Llavor(Planta planta, int temps)
	{
		this.planta = planta;
		this.temps = temps;
	}
	
	public Planta creix()
	{
		if(currentTemps < temps)
		{
			currentTemps++;
		}
		else if (currentTemps == temps)
		{
			this.setViva(false);
			return planta;
		}
		
		return null;
	}
	
	public char getChar(int nivell)
	{
		return '^';
	}
}

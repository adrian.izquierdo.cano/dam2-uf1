import java.util.Random;

public abstract class Planta 
{
	private boolean esViva = true;
	private int altura = 0;

	public char getChar(int nivell) 
	{
		return ' ';
	}
	
	public Planta creix() 
	{
		if (this.getAltura() < 10) 
		{
			this.setAltura(this.getAltura() + 1);
		}
		else
		{
			esViva = false;
			return null;
		}
		return this;	
	}
	
	public int escampaLlavor() 
	{
		int delta = (int) (Math.random() * 4) + 1;
		return delta;
	}
	
	public static Planta[] generarPlantaRandom(int longitut)
	{
		Planta[] result = new Planta[longitut];
		
		for(int i = 0; i < longitut; i++)
		{
			result[i] = getPlantaRandom();
		}
		
		return result;
		
	}
	
	private static Planta getPlantaRandom()
	{
		Random rand = new Random();
		int tipo = rand.nextInt(3);
		
		switch(tipo)
		{
		case 0: 
			return new Altibus();
					
		case 1:
		
			return new Declinus();
					
		case 2:
		
			return new Randomibus();
					
		}
		
		return null;
	}

	public int getAltura() 
	{
		return altura;
	}

	public boolean esViva()
	{
		return esViva;	
	}
	
	public void setViva(boolean viva)
	{
		this.esViva = viva;	
	}

	public void setAltura(int altura)
	{
		this.altura = altura;
	}
	
}
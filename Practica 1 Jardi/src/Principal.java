import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Principal {
	
	public static final Random rand = new Random();
	
	private static Class[] tiposDePlantas = {Altibus.class, Declinus.class, Randomibus.class};
	
	private static int PLANTES_INICIALS = 10 + 1 ;
	private static int AMPLADA_INICIAL = 120;
	
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		
		Jardi jardi = new Jardi(AMPLADA_INICIAL, Planta.generarPlantaRandom(PLANTES_INICIALS), Jardi.getRandomPositions(PLANTES_INICIALS, AMPLADA_INICIAL));
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				jardi.temps();
				System.out.println(jardi);
			}
			
		}, 0, 100);
		
		while(true)
		{
			String line = input.nextLine();
			//SI ESCRIBES "sortir" FINALIZAS EL PROGRAMA
			if(line.equals("sortir"))
			{
				timer.cancel();
				break;
			}
			else
			{
				//SI ESCRIBES EL NOMBRE DE LA CLASSE DE LA PLANTA EN LA CONSOLA
				//APARECE UNA SEMILLA DE ESTA PLANTA EN UNA POSICION ALEATORIA
				for(Class<?> c : tiposDePlantas)
				{
					if(c.toString().toLowerCase().split(" ")[1].equals(line.toLowerCase()))
					{
						try
						{
							jardi.afegirPlanta(new Llavor((Planta) c.newInstance(), 5));
						}
						catch (InstantiationException | IllegalAccessException e)
						{
							// TODO Auto-generated catch block
							//e.printStackTrace();
						}
						
					}
				}
			}
		}
	}
}

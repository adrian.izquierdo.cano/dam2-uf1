import java.util.Random;

public class Randomibus extends Planta 
{
	private Random rand = new Random();
	
	public Planta creix()
	{
		Planta ret = null;
		
		if(rand.nextInt(50) == 0) return ret;
		
		if(rand.nextInt(50) == 0)
		{
			this.setViva(false);
			return ret;
		}
		
		if(this.getAltura() < 10)
		{
			this.setAltura(this.getAltura() + 1);
		}
		
		int chanceToDie = this.getAltura() * 10 - 83;
		
		chanceToDie = chanceToDie >= 0 ? chanceToDie : 0;
		
		if(chanceToDie > rand.nextInt(100))
		{
			this.setViva(false);
			ret = new Llavor(new Randomibus(), rand.nextInt(10) + 1);
		}
		
		if(ret == null && rand.nextInt(100) < chanceToDie + 8) ret = new Llavor(new Randomibus(), rand.nextInt(10) + 1);
		
		return ret;
	}
	
	public char getChar(int nivell)
	{
		nivell++;
		if(nivell < this.getAltura())
		{
			return '#';
		}
		else if(nivell == this.getAltura())
		{
			return '$';
		}
		else
		{
			return ' ';
		}
	}
	
	public int escampaLlavor()
	{
		return rand.nextInt(7) - 3;
	}
}
	


import java.util.Random;
import java.util.Scanner;

public class Utils
{
	public static void shuffleArray(int[] array)
	{
		Random rand = new Random();
		
		for(int i = 0; i < array.length * 2; i++)
		{
			int a = rand.nextInt(array.length);
			int b = rand.nextInt(array.length);
			
			int temp = array[a];
			array[a] = array[b];
			array[b] = temp;
		}
	}
	
	public static void initArray(int[] array)
	{
		for(int i = 0; i < array.length; i++)
		{
			array[i] = i;
		}
	}
}

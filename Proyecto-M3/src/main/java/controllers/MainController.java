package controllers;

import clases.Game;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class MainController 
{
	Game game = Game.getGame();
	@FXML
	private GridPane grid;
	@FXML
	private TextField t0;
	@FXML
	private TextField letras;
	@FXML
	private Button send;
	
	
	@FXML
    public void initialize() 
	{
		getNodeByRowColumnIndex(grid, 1).setText("h");
		getNodeByRowColumnIndex(grid, 1).setId("1");
		
		//getNodeByRowColumnIndex(grid, 1).setVisible(false);
		
		letras.setText(game.getLetters());
    }

	public void check()
	{
		
	}
	public TextField getNodeByRowColumnIndex (/*final int row, final int column,*/ GridPane gridPane,Integer index) {
	    
	    ObservableList<Node> childrens = gridPane.getChildren();

	   /*for (Node node : childrens) {
	        if(GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
	            break;
	        }
	    }*/
	    TextField tf = (TextField) childrens.get(index);
	    

	    return tf;
	}
	
	
}

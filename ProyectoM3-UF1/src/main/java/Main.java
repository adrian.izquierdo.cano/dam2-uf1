
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import clases.Game;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) 
	{
		Application.launch(args);
	}

	public void start(Stage stage) throws IOException
	{
		Parent root = FXMLLoader.load(getClass().getResource("/gui/Main.fxml"));
		Scene scene = new Scene(root);
		stage.setResizable(false);
		stage.setTitle("WoW");
		stage.setScene(scene);
		stage.show();
	}


}

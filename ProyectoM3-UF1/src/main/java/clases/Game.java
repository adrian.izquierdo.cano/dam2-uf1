package clases;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Game 
{
	private static Game game;
	private static Map<String,Map<String,Integer>> diccionario;
	private Game()
	{
		
	}
	
	public static Game getGame()
	{
		if(game == null)
		{
			game = new Game();
		}
		return game;
	}
	
	private void dataHardcoded()
	{
		String letras = "tlpao";
		Map<String,Integer> palabras = new LinkedHashMap<String, Integer>();
		palabras.put("plato", 1);
		palabras.put("talo", 2);
		palabras.put("apto", 3);
		palabras.put("palo", 4);
		palabras.put("pato", 5);
		palabras.put("alto", 6);
		palabras.put("ola", 7);
		Map<String,Map<String,Integer>> partida = new LinkedHashMap<String, Map<String, Integer>>() ;		
		partida.put(letras, palabras);
		
		Game.diccionario = partida;
	}
	public String getLetters()
	{
		Entry<String, Map<String, Integer>> entry = diccionario.entrySet().iterator().next();
		String key = entry.getKey();
		return key;
	}
}

A�adidos:

Altibus:
Este tipo de planta crea 2 semillas en vez de 3, alturas 8 y 9.
Cambio realizado para evitar que haya demasiadas plantas de este tipo.
La escamparLlavor() devuelve un rango diferente segun su altura.

Planta opcional:
Hemos creado un tipo de planta llamado Randomibus que dependiendo de su altura
tiene unas probabilidades de morir y soltar una semilla distintas.


Las primeras plantas generadas son colocadas de forma aleatoria en el jard�n.

En cualquier momento por consola se puede pedir que el programa pare con el comando �sortir�. 

Mediante la consola tambi�n podemos hacer crecer nuevas plantas
poniendo el nombre del tipo de planta el cual queremos que aparezca.

El jard�n no se recorre en orden secuencial, se recorren en orden aleatorio
para que las plantas situadas al final no est�n en desventaja
a la hora de encotrar espacio para sus semillas.


Otros detalles se han cambiado para intentar que el jard�n est� equilibrado,
es decir, no haya plantas que se multipliquen y terminen quitando el espacio a otras.



https://gitlab.com/adrian.izquierdo.cano/dam2-uf1.git
import java.util.Random;

public class MarkJava
{
	Random random;
	int energia;
	boolean haMogut;
	
	public MarkJava(Random random, int energia, boolean haMogut) {
		super();
		this.random = random;
		this.energia = energia;
		this.haMogut = haMogut;
	}
	
	public int obteEnergia()
	{
		return energia;
		
	}
	
	public boolean decideixSiMou()
	{
		if (haMogut == true)
		{
			return false;
		}
		else
		{
			return true;
		}	
	}
	public void gastaEnergia(int energiaGastada) throws IllegalArgumentException
	{
		int res = 0;
		res = this.energia - energiaGastada;
		if(energia < 0)
		{
			throw new IllegalArgumentException();
		}
	}
	
	public void gastaBateria()
	{
		this.energia = 0;
	}
	
	public void interactua(MarkJava unAltreRobot)
	{
		
	}
	
	public void recarregaBateria()
	{
		this.energia = 100;
	}
}

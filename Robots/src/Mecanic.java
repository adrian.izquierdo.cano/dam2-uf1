import java.util.Random;

public class Mecanic extends MarkJava
{

	public Mecanic(Random random, int energia, boolean haMogut) {
		super(random, energia, haMogut);
	}

	public void interactua(MarkJava unAltreRobot)
	{
		if(this.energia >= 4)
		{
			unAltreRobot.recarregaBateria();
			this.energia = this.energia - 1;
		}
		else
		{
			
		}
	}
	
	public boolean decideixSiMou()
	{
		if(this.energia < 5)
		{
			this.recarregaBateria();
			return false;
		}
		else
		{
			Random r = new Random();
			int n = r.nextInt(9);
			n++;
			if(n <= 6)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
}

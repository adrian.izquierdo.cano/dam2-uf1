import java.util.Random;

public class Replicant extends MarkJava
{
	MarkJava robotQueReplicara;

	public Replicant(Random random, int energia, boolean haMogut) {
		super(random, energia, haMogut);
	}
	
	public void interactua(MarkJava unAltreRobot)
	{
		robotQueReplicara = unAltreRobot;
	}

	public boolean decideixSiMou()
	{
		if(this.energia < 7)
		{
			return false;
		}
		else
		{
			Random r = new Random();
			int n = r.nextInt(9);
			n++;
			if(n <= 6)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	public MarkJava construeix()
	{
		if(this.energia < 7)
		{
			this.energia = this.energia - 5;
			return null;
		}
		else
		{
			this.energia = this.energia - 5;
			return robotQueReplicara;
		}
		
	}
}

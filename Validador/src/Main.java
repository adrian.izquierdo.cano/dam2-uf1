import java.util.ArrayDeque;
import java.util.Deque;

public class Main {

	public static void main(String[] args) {
		String f = "2*[(a+b)/5]-7";
		System.out.println(validador(f));
		
	}

	public static boolean validador (String formula)
	{
		Deque<Character> pila = new ArrayDeque<Character>();
		for (char ch: formula.toCharArray()) 
		{
			if (ch == '(' || ch == '[' || ch == '{') 
			{
				pila.push(ch);
			}
			if (ch == ')' || ch == ']' || ch == '}') 
			{
				if(pila.isEmpty())
				{
					return false;
				}
				if (ch == ')' && pila.peek() == '(')
				{
					pila.pop();
				}
				else if (ch == ']' && pila.peek() == '[')
				{
					pila.pop();
				}
				else if (ch == '}' && pila.peek() == '{')
				{
					pila.pop();
				}
				else
				{
					return false;
				}
			}
			
		}	
		if (pila.isEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
}

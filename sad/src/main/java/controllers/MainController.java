package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import clases.Game;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class MainController 
{
	Game game = Game.getGame();
	@FXML
	private Pane pane;
	@FXML
	private GridPane grid;
	@FXML
	private TextField respuesta;
	@FXML
	private Button send;
	@FXML
	private ChoiceBox<String> extras;
	@FXML
	private TextField estado;
	@FXML
	private Button letra1;
	@FXML
	private Button letra2;
	@FXML
	private Button letra3;
	@FXML
	private Button letra4;
	@FXML
	private Button letra5;
	@FXML
	private Button btnshuffle;
	@FXML
	private Button btnpista;
	@FXML
	private Button clear;
	
	List<Button> buttons = new ArrayList<>();
	List<String> palabras = game.palabrasPuzzle();
	@FXML
    public void initialize() 
	{
		buttons.add(letra1);
		buttons.add(letra2);
		buttons.add(letra3);
		buttons.add(letra4);
		buttons.add(letra5);
		
		setLetras();
		setPuzzle();
    }

	@FXML
	public void sendClick(ActionEvent event) 
	{
		Boolean bandera = false;
		
		for ( Entry<String, Integer> entry : game.respuestas().entrySet()) 
		{
		    String palabra = entry.getKey();
		    if(respuesta.getText().toLowerCase().contentEquals(palabra.toLowerCase()))
		    {
		    	palabras.remove(palabra);
		    	Integer palabraId = entry.getValue();
		    	if(palabraId == 0)
		    	{
		    		if(extras.getItems().contains(palabra))
		    		{
		    			estado.setText("Palabra extra repetida");
		    			bandera= true;
		    		}
		    		else
		    		{
		    			extras.getItems().add(palabra);
		    			estado.setText("Palabra extra encontrada");
		    			bandera = true;
		    		}	
		    	}
		    	else if(palabraId > 0)
		    	{
		    		for ( Entry<Integer, Map<Integer, String>> entry1 : game.dataPuzzle().entrySet())
			    	{
			    		Integer palabraNum = entry1.getKey();
			    		if(palabraId == palabraNum)
			    		{
			    			Map<Integer, String> caracteres = entry1.getValue();
			    			for ( Entry<Integer, String> entry2 : caracteres.entrySet())
			    			{
			    				Integer index = entry2.getKey();
			    				String  caracter = entry2.getValue();
			    				getNodeByRowColumnIndex(grid, index).setText(caracter);
			    				getNodeByRowColumnIndex(grid, index).setStyle("-fx-control-inner-background: #8ce366;"
			    						+ "-fx-text-inner-color: white");
			    				estado.setText("Palabra encontrada");
			    				bandera = true;
			    			}
			    			
			    		}
			    	}
		    	}    	
		    }
		}
		if(bandera == false)
		{
			estado.setText("Palabra incorrecta");
		}
		respuesta.setText("");
		for (Button btn : buttons) 
		{
			btn.setDisable(false);
		}
		btnshuffle.setDisable(false);
		if(palabras.isEmpty())
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Winner Winner Chicken Dinner");
			alert.setHeaderText("Parece que has ganado");
			alert.setContentText("En efecto has ganado te mereces un pin");

			alert.showAndWait();
			for (Button btn : buttons) 
			{
				btn.setDisable(true);
			}
			btnshuffle.setDisable(true);
			send.setDisable(true);
			clear.setDisable(true);
			estado.setText("Has ganado");
		}
	}
	public void setPuzzle()
	{
		for (Integer index : game.puzzleIndexes()) 
		{
			getNodeByRowColumnIndex(grid, index).setVisible(true);
		}
	}
	public void setLetras()
	{
		List<String> palabra = game.getLetters();
		Collections.shuffle(palabra);
		int i=0;
		for (Button btn : buttons) 
		{
				btn.setText(palabra.get(i));
				i++;
		} 
	}
	@FXML
	public void shuffle(ActionEvent event) 
	{
		setLetras();
	}
	@FXML
	public void pista(ActionEvent event) 
	{
		Map<Integer, String> mapa = new HashMap<Integer, String>();
		Integer index = 0;
		String letra = "";
		List<Integer> indexes = game.puzzleIndexes();
		Collections.shuffle(indexes);
		Integer randomIndex = indexes.get(0);
		
		if(getNodeByRowColumnIndex(grid, randomIndex).getText().contentEquals(""))
		{
			for ( Entry<Integer, Map<Integer, String>> entry : game.dataPuzzle().entrySet()) 
			{
				mapa = entry.getValue();
				for ( Entry<Integer, String> entry2 : mapa.entrySet()) 
				{
					index = entry2.getKey();
					if(index == randomIndex)
					{
						letra = entry2.getValue();
						getNodeByRowColumnIndex(grid, index).setText(letra);
					}
				}
			}	
		}
	}
	@FXML
	public void letras(ActionEvent event) 
	{
		Button btn = (Button) event.getSource();
		respuesta.setText(respuesta.getText() + btn.getText());
		btn.setDisable(true);
		btnshuffle.setDisable(true);
	}
	@FXML
	public void borrar(ActionEvent event) 
	{
		respuesta.setText("");
		for (Button btn : buttons) 
		{
			btn.setDisable(false);
		}
		btnshuffle.setDisable(false);
	}
	
	public TextField getNodeByRowColumnIndex (GridPane gridPane,Integer index) {
	    
	    ObservableList<Node> childrens = gridPane.getChildren();

	    TextField tf = (TextField) childrens.get(index);
	    
	    return tf;
	}
	
	
}

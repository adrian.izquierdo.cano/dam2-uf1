package main;

import java.io.IOException;

import javafx.application.Application;
import javafx.css.StyleOrigin;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application {

	public static void main(String[] args) 
	{
		Application.launch(args);
	}

	public void start(Stage stage) throws IOException
	{
		Parent root = FXMLLoader.load(getClass().getResource("/gui/Main.fxml"));
		
		Scene scene = new Scene(root);
		stage.initStyle(StageStyle.UTILITY);
		stage.setResizable(false);
		stage.setTitle("WoW");
		stage.setScene(scene);
		stage.show();
	}


}

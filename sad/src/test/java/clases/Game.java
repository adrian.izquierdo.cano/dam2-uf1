package clases;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Game 
{
	private static Game game;
	private static Map<String,Map<String,Integer>> diccionario;
	private Game()
	{
		
	}
	
	public static Game getGame()
	{
		if(game == null)
		{
			game = new Game();
		}
		return game;
	}
	/*palabras*/
	private Map<String,Map<String,Integer>> dataHardcoded()
	{
		String letras = "ATLPA";
		Map<String,Integer> palabras = new LinkedHashMap<String, Integer>();
		palabras.put("tapa", 1);
		palabras.put("alta", 2);
		palabras.put("pata", 3);
		palabras.put("tala", 4);
		palabras.put("plata", 5);
		palabras.put("pala", 6);
		palabras.put("talpa", 0);
		palabras.put("lata", 0);
		palabras.put("tal", 0);
		palabras.put("ala", 0);
		palabras.put("apta", 0);
		palabras.put("lapa", 0);
		palabras.put("pal", 0);
		palabras.put("palta", 0);
		palabras.put("apa", 0);
		
		Map<String,Map<String,Integer>> partida = new LinkedHashMap<String, Map<String, Integer>>() ;		
		partida.put(letras, palabras);
		
		return partida;	
	}
	public Map<String,Integer> respuestas()
	{
		Map<String, Integer> resultado = new LinkedHashMap<String, Integer>();
		for ( Entry<String, Map<String, Integer>> entry : dataHardcoded().entrySet()) 
		{
		    resultado = entry.getValue(); 
		}
		return resultado;
	}
	public List<String> palabrasExtra()
	{
		List<String> palabrasExtra = new ArrayList<String>();
		for ( Entry<String, Integer> entry : respuestas().entrySet()) 
		{
		    String palabra = entry.getKey();
		    palabrasExtra.add(palabra);
		}
		return palabrasExtra;
	}
	/*Puzzle*/
	public Map<Integer,Map<Integer,String>> dataPuzzle()
	{
		Map<Integer,Map<Integer,String>> puzzle = new HashMap<Integer,Map<Integer,String>>();
		Map<Integer, String> letras1 = new HashMap<Integer, String>();
		Map<Integer, String> letras2 = new HashMap<Integer, String>();
		Map<Integer, String> letras3 = new HashMap<Integer, String>();
		Map<Integer, String> letras4 = new HashMap<Integer, String>();
		Map<Integer, String> letras5 = new HashMap<Integer, String>();
		Map<Integer, String> letras6 = new HashMap<Integer, String>();
		letras1.put(23, "T");
		letras1.put(24, "A");
		letras1.put(25, "P");
		letras1.put(26, "A");
		
		letras2.put(42, "A");
		letras2.put(43, "L");
		letras2.put(44, "T");
		letras2.put(45, "A");
		
		letras3.put(64, "P");
		letras3.put(65, "A");
		letras3.put(66, "T");
		letras3.put(67, "A");
		
		letras4.put(23, "T");
		letras4.put(33, "A");
		letras4.put(43, "L");
		letras4.put(53, "A");
		
		letras5.put(25, "P");
		letras5.put(35, "L");
		letras5.put(45, "A");
		letras5.put(55, "T");
		letras5.put(65, "A");
		
		letras6.put(57, "P");
		letras6.put(67, "A");
		letras6.put(77, "L");
		letras6.put(87, "A");
		
		
		puzzle.put(1, letras1);
		puzzle.put(2, letras2);
		puzzle.put(3, letras3);
		puzzle.put(4, letras4);
		puzzle.put(5, letras5);
		puzzle.put(6, letras6);
		
		return puzzle;
	}
	
	public List<Integer> puzzleIndexes()
	{
		Map<Integer, String> mapa = new HashMap<Integer, String>();
		List<Integer> listaIndex = new ArrayList<Integer>();
		for ( Entry<Integer, Map<Integer, String>> entry : dataPuzzle().entrySet()) 
		{
			mapa = entry.getValue();
			for ( Entry<Integer, String> entry2 : mapa.entrySet()) 
			{
				Integer integer = entry2.getKey();
				listaIndex.add(integer);
			}
		}
		
		return listaIndex;
	}
	public String getLetters()
	{
		dataHardcoded();
		String letters = "";
		for ( String key : dataHardcoded().keySet() ) 
		{
		    letters = key;
		}
		return letters;	
	}
}

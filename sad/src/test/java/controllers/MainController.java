package controllers;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import clases.Game;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class MainController 
{
	Game game = Game.getGame();
	@FXML
	private Pane pane;
	@FXML
	private GridPane grid;
	@FXML
	private TextField letras;
	@FXML
	private TextField respuesta;
	@FXML
	private Button send;
	@FXML
	private ChoiceBox<String> extras;
	@FXML
	private TextField estado;
	
	@FXML
    public void initialize() 
	{
		setPuzzle();
		letras.setText(game.getLetters());
    }

	@FXML
	public void sendClick(ActionEvent event) 
	{
		/*Button button = (Button) event.getSource();*/
		Boolean bandera = false;
		for ( Entry<String, Integer> entry : game.respuestas().entrySet()) 
		{
		    String palabra = entry.getKey();
		    if(respuesta.getText().toLowerCase().contentEquals(palabra.toLowerCase()))
		    {
		    	Integer palabraId = entry.getValue();
		    	if(palabraId == 0)
		    	{
		    		if(extras.getItems().contains(palabra))
		    		{
		    			estado.setText("Palabra extra repetida");
		    			bandera= true;
		    		}
		    		else
		    		{
		    			extras.getItems().add(palabra);
		    			estado.setText("Palabra extra encontrada");
		    			bandera = true;
		    		}	
		    	}
		    	else if(palabraId > 0)
		    	{
		    		for ( Entry<Integer, Map<Integer, String>> entry1 : game.dataPuzzle().entrySet())
			    	{
			    		Integer palabraNum = entry1.getKey();
			    		if(palabraId == palabraNum)
			    		{
			    			Map<Integer, String> caracteres = entry1.getValue();
			    			for ( Entry<Integer, String> entry2 : caracteres.entrySet())
			    			{
			    				Integer index = entry2.getKey();
			    				String  caracter = entry2.getValue();
			    				getNodeByRowColumnIndex(grid, index).setText(caracter);
			    				getNodeByRowColumnIndex(grid, index).setStyle("-fx-control-inner-background: #8ce366;"
			    						+ "-fx-text-inner-color: white");
			    				estado.setText("Palabra encontrada");
			    				bandera = true;
			    			}
			    		}
			    	}
		    	}    	
		    }
		}
		if(bandera == false)
		{
			estado.setText("Palabra incorrecta");
		}
		respuesta.setText("");
	}
	public void setPuzzle()
	{
		for (Integer index : game.puzzleIndexes()) 
		{
			getNodeByRowColumnIndex(grid, index).setVisible(true);
		}
	}
	public TextField getNodeByRowColumnIndex (GridPane gridPane,Integer index) {
	    
	    ObservableList<Node> childrens = gridPane.getChildren();

	    TextField tf = (TextField) childrens.get(index);
	    
	    return tf;
	}
	
	
}
